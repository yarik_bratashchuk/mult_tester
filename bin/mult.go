package main

import (
	"./app"
	"./config"
	"./scgi"
	"fmt"
	"net"
	"net/http"
	"net/http/fcgi"
	"os"
	"runtime"
	"time"
)

const (
	VERSION = `0.0.1`
	NAME    = `Multiplex_user`

	PROTOCOL_HTTP = "http"
	PROTOCOL_FCGI = "fcgi"
	PROTOCOL_SCGI = "scgi"

	C0 = "\x1b[30;1m"
	CR = "\x1b[31;1m"
	CG = "\x1b[32;1m"
	CY = "\x1b[33;1m"
	CB = "\x1b[34;1m"
	CM = "\x1b[35;1m"
	CC = "\x1b[36;1m"
	CW = "\x1b[37;1m"
	CN = "\x1b[0m"
)

type XcgiHandler struct{}

func printProcInfo() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	hostname, _ := os.Hostname()
	fmt.Printf(CW+"Goversion: %s, Goroutine: %d, NumCPU: %d, Hostname: %s\n"+CN,
		runtime.Version(), runtime.NumGoroutine(), runtime.NumCPU(), hostname)
}

func main() {

	fmt.Printf(CW+"%s: %s\n"+CN, NAME, VERSION)
	printProcInfo()
	conf := config.GetConfig()
	listener, err := net.Listen("tcp", conf.Network.Bind)

	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("STARTING PHANTOM")
	app.Phantom()
	fmt.Println("FINISHED")

	fmt.Println(CC+conf.Network.Bind, conf.Network.Protocol+CN)

	switch conf.Network.Protocol {
	case PROTOCOL_FCGI:
		var fcgi_handler XcgiHandler
		fcgi.Serve(listener, fcgi_handler)
	case PROTOCOL_SCGI:
		var scgi_handler XcgiHandler
		scgi.Serve(listener, scgi_handler)
	case PROTOCOL_HTTP:
		allow := []string{
			`/fonts/`,
			`/css/`,
			`/img/`,
			`/js/`,
			`/data/`,
			`/robots.txt`,
			`/favicon.png`,
		}
		for _, path := range allow {
			http.Handle(path, http.FileServer(http.Dir(conf.Http.Htdocs)))
		}
		http.HandleFunc("/", handler)
		http_server := &http.Server{
			ReadTimeout:    30 * time.Second,
			WriteTimeout:   60 * time.Second,
			MaxHeaderBytes: 1 << 16,
		}
		http_server.Serve(listener)
	default:
		fmt.Println("Protocol '" + conf.Network.Protocol + "' not support")
	}

}

func (XcgiHandler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	app.Handler(res, req)
}

func handler(res http.ResponseWriter, req *http.Request) {
	app.Handler(res, req)
}
