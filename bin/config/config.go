package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

const (
	APP_CONFIG_NAME         = "/../etc/config.conf"
	APP_TESTING_CONFIG_NAME = "/../etc/testing_config/"
)

type Config struct {
	Process struct {
		Daemon bool   `json:"daemon"`
		ChRoot string `json:"chroot"`
	} `json:"process"`

	Network struct {
		Bind     string `json:"bind"`
		Protocol string `json:"protocol"`
	} `json:"network"`

	Http struct {
		Htdocs   string `json:"htdocs"`
		Template string `json:"template"`
	} `json:"http"`

	Mysql struct {
		Connection string `json:"connection"`
	} `json:"mysql"`
	TestResults string
	Site        string `json:"site"`
}

type TestingConfig map[string]PageConfig

type PageConfig struct {
	Name         string   `json:"Name"`
	Path         string   `json:"Path"`
	HrefPatterns []string `json:"AnchorPatterns"`
	Selectors    []struct {
		Selector string `json:"selector"`
		Quantity int    `json:"quantity"`
	} `json:"Selectors"`
}

var app_conf *Config
var testing_conf TestingConfig
var cwd string

func init() {
	cwd, _ = os.Getwd()
}

func GetConfig() Config {

	if app_conf == nil {
		app_conf = new(Config)

		if _, err := os.Stat(cwd + APP_CONFIG_NAME); err == nil {
			app_conf.load(cwd + APP_CONFIG_NAME)
		} else {
			base_dir := os.Args[0]
			os.Chdir(filepath.Dir(base_dir))
			app_conf.load(cwd + APP_CONFIG_NAME)
		}
	}
	return *app_conf
}

func (conf *Config) load(filename string) {

	data, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}

	conf.TestResults = cwd + "/../etc/testing_results"

	err = json.Unmarshal(data, &conf)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
}

func GetTestingConfig() TestingConfig {

	testing_conf = make(TestingConfig, 0)

	files, err := ioutil.ReadDir(cwd + APP_TESTING_CONFIG_NAME)
	if err != nil {
		fmt.Println(err)
	}
	for _, file := range files {
		testing_conf[strings.Split(file.Name(), ".")[0]] = loadPageConfig(file.Name())
	}

	return testing_conf
}

func GetAnchorPatterns() []string {
	patterns := make([]string, 0)
	configs := GetTestingConfig()
	for _, conf := range configs {
		patterns = append(patterns, conf.Path)
	}
	return patterns
}

func loadPageConfig(filename string) PageConfig {
	pageConf := &PageConfig{}
	data, err := ioutil.ReadFile(cwd + APP_TESTING_CONFIG_NAME + filename)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}

	err = json.Unmarshal(data, &pageConf)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
	return *pageConf
}
