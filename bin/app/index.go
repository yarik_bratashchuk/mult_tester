package app

import (
	"../model"
)

//Index handles requests to /
func Index(c *model.Client) {

	page := model.Render{
		Res:  c.Res,
		Tmpl: "index/index.html",
		Data: struct {
		}{},
	}

	page.Render()
}
