package app

import (
	"../config"
	// "../model"
	"fmt"
	"github.com/sclevine/agouti"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"regexp"
	"strings"
	"time"
)

var (
	testConf       config.TestingConfig
	genConf        config.Config
	anchorPatterns []string
)

func init() {
	testConf = config.GetTestingConfig()
	genConf = config.GetConfig()
	anchorPatterns = config.GetAnchorPatterns()
}

//Phantom emulates users visits on new.multiplex.ua
func Phantom() {
	driver := agouti.PhantomJS()
	if err := driver.Start(); err != nil {
		fmt.Println("Failed to start driver:%v", err)
	}
	defer driver.Stop()
	page, err := driver.NewPage(agouti.Browser("phantomjs"))
	if err != nil {
		fmt.Println("Failed to open page:%v", err)
	}
	page.Size(1280, 1024)

	moveToPage(page, "http://"+genConf.Site)
	walk(page, 100)
}

func walk(page *agouti.Page, moves int) {
	for i := 0; i < moves; i++ {
		allAnchors := page.All("a")
		sortedAnchors := getAnchors(allAnchors, anchorPatterns)
		myrand := random(0, len(sortedAnchors))
		checkPage(page)
		href, _ := sortedAnchors[myrand].Attribute("href")
		moveToPage(page, href)
	}
}

func getAnchors(allAnchors *agouti.MultiSelection, anchorPatterns []string) [](*agouti.Selection) {
	anchors := make([](*agouti.Selection), 0)
	for i, _ := allAnchors.Selection.Count(); i >= 0; i-- {
		a := allAnchors.At(i)
		for _, pattern := range anchorPatterns {
			href, _ := a.Attribute("href")
			if matched, _ := regexp.MatchString("^(http://"+genConf.Site+")?/"+pattern, href); matched {
				fmt.Println(href)
				anchors = append(anchors, a)
			}
		}
	}
	return anchors
}

func checkPage(page *agouti.Page) {
	var (
		selectorResults string
		anchorsResults  string
		conf            config.PageConfig
		dir             string
	)
	url, _ := page.URL()
	for _, pageConf := range testConf {
		if matched, _ := regexp.MatchString("^(http://"+genConf.Site+")?/"+pageConf.Path, url); matched {
			conf = pageConf
		}
	}
	dir = genConf.TestResults + "/" + conf.Name

	//hiding how to use site popup
	if conf.Name == "main" {
		tour_wr := page.Find(".tour_wr")
		if display, _ := tour_wr.CSS("display"); display != "none" {
			page.Find(".tour_close").Click()
		}
		//hiding check cinema popup
		geo_weil := page.Find(".geo_veil")
		if display, _ := geo_weil.CSS("display"); display == "block" {
			page.Find(".yes").Click()
		}
	}

	if err := os.RemoveAll(dir); err != nil {
		fmt.Println(err)
	}

	if err := os.Mkdir(dir, 0777); err != nil {
		fmt.Println("Failed to create directory", err)
	}

	// checking selectors
	succes := 0
	succesDesired := 0
	for _, selector := range conf.Selectors {
		succesDesired += selector.Quantity
		if found, _ := page.All(selector.Selector).Selection.Count(); selector.Quantity <= found {
			count := found
			selectorResults += fmt.Sprintf("%s: OK (%v <= %v)\n", selector.Selector, selector.Quantity, count)
			succes += selector.Quantity
		} else {
			count := found
			selectorResults += fmt.Sprintf("%s: BAD (%v >= %v)\n", selector.Selector, selector.Quantity, count)
		}
	}
	printSucces(succes, succesDesired, "selectors")

	// checking links
	succes = 0
	succesDesired = len(conf.HrefPatterns)
	for i, _ := page.All("a").Selection.Count(); i >= 0; i-- {
		a := page.All("a").At(i)
		for _, href := range conf.HrefPatterns {
			a, _ := a.Attribute("href")
			if found := strings.Contains(a, href); found {
				succes += 1
				anchorsResults += href + ": \n - " + a + "\n"
			}
		}
	}
	printSucces(succes, succesDesired, "anchors")

	//write selectors to file
	if err := ioutil.WriteFile(dir+"/selectors.txt", []byte(selectorResults), 0777); err != nil {
		fmt.Println("Unable to write to a file: ", err)
	}
	//write anchors to file
	if err := ioutil.WriteFile(dir+"/anchors.txt", []byte(anchorsResults), 0777); err != nil {
		fmt.Println("Unable to write to a file: ", err)
	}
	//making a screenshot
	if err := page.Screenshot(dir + "/" + conf.Name + ".jpg"); err != nil {
		fmt.Println("Unable to make a screenshot: ", err)
	}
}

func moveToPage(page *agouti.Page, url string) {
	startLoading := time.Now()
	fmt.Println(url)
	resp, _ := http.Get(url)
	defer resp.Body.Close()
	if err := page.Navigate(url); err != nil {
		fmt.Println("Failed to navigate:%v", err)
	}
	loadingTime := fmt.Sprintf("%v", time.Since(startLoading).Nanoseconds()/1e6)
	fmt.Println("\nPage: " + url + " - " + loadingTime + " ms\t\t" + resp.Status)
}

func printSucces(succes, succesDesired int, selectors string) {
	if succes >= succesDesired {
		fmt.Println(" -" + selectors + ": OK")
	} else {
		fmt.Println(" -" + selectors + ": BAD")
	}
}

func random(min, max int) int {
	rand.Seed(int64(time.Now().Nanosecond() / 1e6))
	return rand.Intn(max-min) + min
}

// func testPage(page *agouti.Page, anchor *agouti.Selection) {
// 	href, _ := anchor.Attribute("href")
// 	if newMult := strings.Contains(href, genConf.Site); !newMult {
// 		return
// 	}
// 	resp, _ := http.Get(href)
// 	defer resp.Body.Close()
// 	startLoading := time.Now()
// 	anchor.Click()
// 	// anchor.Click()
// 	// if err := page.Navigate(href); err != nil {
// 	// 	fmt.Println("Failed to navigate:%v", err)
// 	// }
// 	loadingTime := fmt.Sprintf("%v", time.Since(startLoading).Nanoseconds()/1e6)
// 	if err := page.Screenshot(genConf.TestResults + "/" + href + ".jpg"); err != nil {
// 		fmt.Println("Unable to make a screenshot: ", err)
// 	}
// 	title, _ := page.Title()
// 	url, _ := page.URL()
// 	fmt.Println("\nPage: " + title + " - " + loadingTime + " ms\t\t" + resp.Status + "\t\t(" + url + ")")
// }
