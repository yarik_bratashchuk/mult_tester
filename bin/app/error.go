package app

import (
	"../model"
	"net/http"
)

func MethodNotAllowed(c *model.Client) {

	c.Res.Header().Add(`Content-Type`, `text/html`)
	c.Res.WriteHeader(http.StatusMethodNotAllowed)
	page := model.Render{Res: c.Res, Tmpl: "errors/405.html"}
	page.Render()
}

func ServiceUnavailable(c *model.Client) {

	c.Res.WriteHeader(http.StatusServiceUnavailable)
	c.Res.Header().Add(`Content-Type`, `text/html`)
	page := model.Render{Res: c.Res, Tmpl: "errors/503.html"}
	page.Render()
}

func NotFound(c *model.Client) {

	c.Res.Header().Add(`Content-Type`, `text/html`)
	c.Res.WriteHeader(http.StatusNotFound)
	page := model.Render{Res: c.Res, Tmpl: "errors/404.html"}
	page.Render()
}

func InternalServerError(c *model.Client) {

	c.Res.Header().Add(`Content-Type`, `text/html`)
	c.Res.WriteHeader(http.StatusInternalServerError)
	page := model.Render{Res: c.Res, Tmpl: "errors/500.html"}
	page.Render()
}
