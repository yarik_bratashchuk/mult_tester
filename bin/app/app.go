package app

import (
	"../model"
	"../session"
	"fmt"
	"github.com/bradfitz/gomemcache/memcache"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"
)

type (
	cb_fn func(cb *model.Client)
)

var (
	page_callback map[string]cb_fn
	mc            *memcache.Client
)

func init() {

	page_callback = map[string]cb_fn{
		``: Index,
		// `phantom`: Phantom,
	}

	mc = memcache.New("127.0.0.1:11211")

	err := mc.Set(&memcache.Item{
		Key:        "test",
		Value:      []byte("test value"),
		Expiration: 5,
	})

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	session.Init(mc)

}

func Handler(res http.ResponseWriter, req *http.Request) {

	cb := model.Client{
		Start: time.Now().UnixNano(),
		Path:  strings.Split(strings.Trim(req.URL.Path, `/`), `/`),
		Res:   res,
		Req:   req,
	}

	defer error_check(&cb)

	if user, ok := session.GetUser(req); ok {
		cb.User = &user
	}

	if len(cb.Path) == 0 {

		Index(&cb)

	} else {

		if fn, ok := page_callback[cb.Path[0]]; ok {

			fn(&cb)

		}
	}

	cb.Stop = time.Now().UnixNano()
	fmt.Printf("%.03f %v\n", float32(cb.Stop-cb.Start)/1000000.0, cb.Path)
}

func error_check(cb *model.Client) {

	if err := recover(); err != nil {

		pc, file, line, ok := runtime.Caller(4)

		if !ok {
			file = "?"
			line = 0
		}

		fn_name := ""
		fn := runtime.FuncForPC(pc)

		if fn == nil {
			fn_name = "?()"
		} else {
			dot_name := filepath.Ext(fn.Name())
			fn_name = strings.TrimLeft(dot_name, ".") + "()"
		}

		err_str := fmt.Sprintf("%s:%d %s: %s\n", file, line, fn_name, err)

		fmt.Println(err_str)

		InternalServerError(cb)

		var buf [10240]byte
		runtime.Stack(buf[:], false)
		fmt.Printf("%s\n", buf)
	}
}
