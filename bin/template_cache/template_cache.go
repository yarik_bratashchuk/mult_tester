package template_cache

import (
	"../utils"
	"fmt"
	"html/template"
	"io/ioutil"
	"os"
	"regexp"
	"strings"
	"sync"
	"time"
)

type TemplateData struct {
	ctime, mtime, size int64
	tmpl               *template.Template
}

type CacheTemplate struct {
	lock           sync.RWMutex
	template_ttl   int64
	template_path  string
	template_cache map[string]*TemplateData
	function_map   template.FuncMap
}

var (
	templateTtl     int64 = 1
	templateNocache bool  = true

	Month = map[bool]map[string][]string{
		true: map[string][]string{
			"ru": []string{
				"Января",
				"Февраля",
				"Марта",
				"Апреля",
				"Мая",
				"Июня",
				"Июля",
				"Августа",
				"Сентября",
				"Октября",
				"Ноября",
				"Декабря",
			},
			"ua": []string{
				"Січня",
				"Лютого",
				"Березня",
				"Квітня",
				"Травня",
				"Червня",
				"Липня",
				"Серпня",
				"Вересня",
				"Жовтня",
				"Листопада",
				"Грудня",
			},
		},
		false: map[string][]string{
			"ru": []string{
				"Январь",
				"Февраль",
				"Март",
				"Апрель",
				"Май",
				"Июнь",
				"Июль",
				"Август",
				"Сентябрь",
				"Октябрь",
				"Ноябрь",
				"Декабрь",
			},
			"ua": []string{
				"Січень",
				"Лютий",
				"Березень",
				"Квітень",
				"Травень",
				"Червень",
				"Липень",
				"Серпень",
				"Вересень",
				"Жовтень",
				"Листопад",
				"Грудень",
			},
		},
	}
)

func CreateCache(template_path string) *CacheTemplate {

	c := new(CacheTemplate)

	c.template_ttl = templateTtl
	c.template_path = template_path
	c.template_cache = make(map[string]*TemplateData)

	tagsRe := regexp.MustCompile("<([^>]+)>")

	c.function_map = template.FuncMap{
		"neInt64": func(Id int64, val int) bool {
			return Id != int64(val)
		},
		"getSources": func(s string) []string {
			return strings.Split(s, "<splitter>")
		},
		"get": func(chunk string) bool {
			return chunk[1:4] != "img"
		},
		"for": func(start int, end int) []int {
			var values []int
			for i := start; i < end; i++ {
				values = append(values, i)
			}
			return values
		},
		"noescape": func(s string) template.HTML {
			return template.HTML(s)
		},
		"text": func(s string) string {
			return tagsRe.ReplaceAllString(s, "")
		},
		"DateTime": func() string {
			tm := time.Now()
			year, month, day := tm.Date()
			return fmt.Sprintf("%02d.%02d.%04d %02d:%02d", day, month, year, tm.Hour(), tm.Minute())
		},
		"Date": func() string {
			tm := time.Now()
			year, month, day := tm.Date()
			return fmt.Sprintf("%02d.%02d.%04d", day, month, year)
		},
		"dateTimeFormat": func(ut int64) string {
			tm := time.Unix(ut, 0)
			year, month, day := tm.Date()
			return fmt.Sprintf("%02d.%02d.%04d %02d:%02d", day, month, year, tm.Hour(), tm.Minute())
		},
		"dateFormat": func(ut int64) string {
			year, month, day := time.Unix(ut, 0).Date()
			return fmt.Sprintf("%02d.%02d.%04d", day, month, year)
		},
		"dayFormat": func(ut int64) string {
			_, _, day := time.Unix(ut, 0).Date()
			return fmt.Sprintf("%d", day)
		},
		"daysFormat": func(first_ut, last_ut int64, m int, lang string, typ bool) string {
			_, _, first_day := time.Unix(first_ut, 0).Date()
			_, _, last_day := time.Unix(last_ut, 0).Date()
			return fmt.Sprintf("%d - %d %s", first_day, last_day, Month[typ][lang][m-1])
		},
		"textFormat": func(text string) template.HTML {
			text = template.HTMLEscapeString(text)
			return template.HTML(strings.Replace(text, "\n", "<br>", -1))
		},
		"toLower": func(text string) string {
			return strings.ToLower(text)
		},
		"add": func(a, b int) int {
			return a + b
		},
		"ip": func(ip uint32) string {
			return utils.Uint32ToIp(ip)
		},
		"makeDate": func(d, m, y int) string {
			return fmt.Sprintf("%02d.%02d.%04d", d, m, y)
		},
		"dayMonthLink": func(d, m int) string {
			return fmt.Sprintf("/%02d/%02d/", m, d)
		},
		"monthToString": func(m int, lang string, typ bool) string {
			return Month[typ][lang][m-1]
		},
		"timeFormatStr": func(ut int64) string {
			tm := time.Unix(ut, 0)
			return fmt.Sprintf("%02d:%02d", tm.Hour(), tm.Minute())
		},
		"dayMonthYearLink": func(d, m, y int) string {
			return fmt.Sprintf("/%04d/%02d/%02d/", y, m, d)
		},
	}

	return c
}

func (c *CacheTemplate) _find(name string) (data *TemplateData, ok bool) {

	c.lock.RLock()
	defer c.lock.RUnlock()

	data, ok = c.template_cache[name]

	if ok && data.ctime < time.Now().Unix() {
		stat, _ := os.Stat(c._file(name))
		if data.mtime != stat.ModTime().Unix() || data.size != stat.Size() {
			delete(c.template_cache, name)
		} else {
			data.ctime = time.Now().Unix() + c.template_ttl
			c.template_cache[name] = data
		}
	}

	return
}

func (c *CacheTemplate) _file(name string) string {

	return c.template_path + name
}

func (c *CacheTemplate) _load(name string) (*TemplateData, error) {

	c.lock.Lock()
	defer c.lock.Unlock()

	file_name := c._file(name)

	text, _ := ioutil.ReadFile(file_name)

	html := string(text)
	html = HtmlMinify(&html)

	tmpl, err := template.New(file_name).Funcs(c.function_map).Parse(html)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	data := TemplateData{
		tmpl: tmpl,
	}

	stat, err := os.Stat(file_name)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	data.size = stat.Size()
	data.mtime = stat.ModTime().Unix()
	data.ctime = time.Now().Unix() + c.template_ttl

	c.template_cache[name] = &data

	return &data, nil
}

func (c *CacheTemplate) Get(name string) (*template.Template, bool) {

	if templateNocache {
		data, err := c._load(name)
		if err == nil {
			return data.tmpl, true
		}
		fmt.Println(err)
		return nil, false
	}

	data, ok := c._find(name)
	if ok {
		return data.tmpl, true
	} else {
		data, err := c._load(name)
		if err == nil {
			return data.tmpl, true
		}
	}
	return nil, false
}
