#!/bin/bash

(GOOS=linux GOARCH=386 CGO_ENABLED=0 go build -o filename filename.go; cat filename) | ssh <USER>@<HOST> 'cd /path/to/folder/named/bin; cat > ./filename.tmp; chmod +x ./filename.tmp; mv ./filename.tmp ./filename; killall filename; sleep 2; (setsid nohup ./filename > /dev/null 2>&1 &)'