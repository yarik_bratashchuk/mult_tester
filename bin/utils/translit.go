package utils

import (
	"regexp"
	"strings"
)

var (
	trDict = []string{
		`а`, `a`,
		`б`, `b`,
		`в`, `v`,
		`г`, `g`,
		`д`, `d`,
		`е`, `e`,
		`ё`, `e`,
		`ж`, `j`,
		`з`, `z`,
		`и`, `i`,
		`й`, `y`,
		`к`, `k`,
		`л`, `l`,
		`м`, `m`,
		`н`, `n`,
		`о`, `o`,
		`п`, `p`,
		`р`, `r`,
		`с`, `s`,
		`т`, `t`,
		`у`, `u`,
		`ф`, `f`,
		`х`, `h`,
		`ц`, `ts`,
		`ч`, `ch`,
		`ш`, `sh`,
		`щ`, `sch`,
		`ъ`, `y`,
		`ы`, `yi`,
		`ь`, ``,
		`э`, `e`,
		`ю`, `yu`,
		`я`, `ya`,
		`і`, `i`,
		`ї`, `yi`,
		`ґ`, `g`,
		`є`, `e`,
		`.`, ``,
		`,`, ``,
		`;`, ``,
		`+`, ``,
		`&`, ``,
		`?`, ``,
		`!`, ``,
		`'`, ``,
	}

	trReplacer = strings.NewReplacer(trDict...)
	reClear    = regexp.MustCompile(`[^-a-z0-9_]+`)
	reDash     = regexp.MustCompile(`-+`)
)

func Translit(str string) string {
	str = strings.ToLower(str)
	str = strings.Trim(str, "\r\n\t ")
	str = trReplacer.Replace(str)
	str = reClear.ReplaceAllString(str, "-")
	str = reDash.ReplaceAllString(str, "-")
	return strings.Trim(str, "-")
}
