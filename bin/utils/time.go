package utils

import (
	"regexp"
	"time"
)

func DaysInMonth(year, month int) int {

	return time.Date(year, time.Month(month+1), 0, 0, 0, 0, 0, time.UTC).Day()
}

func DateToDays(year, month, day int) int {
	s := 365*year + 31*(month-1) + day
	if month <= 2 {
		year--
	} else {
		s -= (month*4 + 23) / 10
	}
	return s + year/4 - (year/100+1)*3/4
}

func DaysToDate(days int) (year, month, day int) {
	var _month time.Month
	year, _month, day = time.Unix(int64((days-719528)*86400), 0).Date()
	month = int(_month)
	return
}

func TimeToDays(tm time.Time) int {
	year, _month, day := tm.Date()
	return DateToDays(year, int(_month), day)
}

func DaysToTime(days int) time.Time {
	return time.Unix((int64(days)-719528)*86400, 0)
}

func UnixToDays(utm int64) int {
	year, _month, day := time.Unix(utm, 0).Date()
	return DateToDays(year, int(_month), day)
}

func DaysToUnix(days int) int64 {
	return (int64(days) - 719528) * 86400
}

func FormatDate(ut int64) (year, month, day int) {
	var _month time.Month
	tm := time.Unix(ut, 0)
	year, _month, day = tm.Date()
	month = int(_month)
	return
}

var (
	parseDateRegexp *regexp.Regexp = regexp.MustCompile(`^(\d{2}).(\d{2}).(\d{4})$`)
)

func ParseDate(dateStr string) (unixTime int64) { // 30.03.2015

	if result := parseDateRegexp.FindStringSubmatch(dateStr); result != nil {

		var (
			location *time.Location
			err      error
		)

		if location, err = time.LoadLocation("Europe/Kiev"); err != nil {
			location = time.UTC
		}

		day, month, year := result[1], result[2], result[3]

		tm := time.Date(ToInt(year), time.Month(ToInt(month)), ToInt(day), 0, 0, 0, 0, location)

		unixTime = tm.Unix()
	}

	return
}
