package utils

import (
	"math/rand"
	"strconv"
	"strings"
	"time"
)

const (
	ALPHA        = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
	ALPHA_LENGHT = len(ALPHA)
	TRIM         = " \r\n\t"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func RandomString(strlen int) string {
	buf := make([]byte, strlen)
	for i := 0; i < strlen; i++ {
		buf[i] = ALPHA[rand.Intn(ALPHA_LENGHT)]
	}
	return string(buf)
}

func ToUint(val string) uint {
	u64, _ := strconv.ParseUint(val, 10, 32)
	return uint(u64)
}

func ToInt(val string) int {
	i64, _ := strconv.ParseInt(val, 10, 32)
	return int(i64)
}

func ToUint64(val string) uint64 {
	u64, _ := strconv.ParseUint(val, 10, 64)
	return u64
}

func ToInt64(val string) int64 {
	i64, _ := strconv.ParseInt(val, 10, 64)
	return i64
}
func ToFloat64(val string) float64 {
	f64, _ := strconv.ParseFloat(val, 64)
	return f64
}
func ToSliceInt(strArray []string) (tmp []int) {
	tmp = make([]int, 0, len(strArray))
	for _, s := range strArray {
		s = strings.Trim(s, TRIM)
		if val, err := strconv.ParseInt(s, 10, 32); err == nil {
			tmp = append(tmp, int(val))
		}
	}
	return
}

func ToSliceInt64(strArray []string) (tmp []int64) {
	tmp = make([]int64, 0, len(strArray))
	for _, s := range strArray {
		s = strings.Trim(s, TRIM)
		if val, err := strconv.ParseInt(s, 10, 64); err == nil {
			tmp = append(tmp, val)
		}
	}
	return
}

func ToSliceUint(strArray []string) (tmp []uint) {
	tmp = make([]uint, 0, len(strArray))
	for _, s := range strArray {
		s = strings.Trim(s, TRIM)
		if val, err := strconv.ParseUint(s, 10, 32); err == nil {
			tmp = append(tmp, uint(val))
		}
	}
	return
}

func ToSliceUint64(strArray []string) (tmp []uint64) {
	tmp = make([]uint64, 0, len(strArray))
	for _, s := range strArray {
		s = strings.Trim(s, TRIM)
		if val, err := strconv.ParseUint(s, 10, 64); err == nil {
			tmp = append(tmp, val)
		}
	}
	return
}

func ToSliceFloat32(strArray []string) (tmp []float32) {
	tmp = make([]float32, 0, len(strArray))
	for _, s := range strArray {
		s = strings.Trim(s, TRIM)
		if val, err := strconv.ParseFloat(s, 32); err == nil {
			tmp = append(tmp, float32(val))
		}
	}
	return
}

func ToSliceFloat64(strArray []string) (tmp []float64) {
	tmp = make([]float64, 0, len(strArray))
	for _, s := range strArray {
		s = strings.Trim(s, TRIM)
		if val, err := strconv.ParseFloat(s, 64); err == nil {
			tmp = append(tmp, val)
		}
	}
	return
}
