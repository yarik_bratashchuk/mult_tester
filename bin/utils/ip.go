package utils

import (
	"bytes"
	"encoding/binary"
	"net"
)

func IpToUint32(ipStr string) (ip uint32) {

	if ipByte := net.ParseIP(ipStr).To4(); ipByte != nil {
		ip4 := []byte(ipByte)
		binary.Read(bytes.NewReader(ip4), binary.LittleEndian, &ip)
	}
	return
}

func Uint32ToIp(ip uint32) (ipStr string) {

	ipByte := make([]byte, 4)
	binary.LittleEndian.PutUint32(ipByte, ip)
	ipStr = net.IPv4(ipByte[0], ipByte[1], ipByte[2], ipByte[3]).String()
	return
}
