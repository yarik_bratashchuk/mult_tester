package model

import (
	"../utils"
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"
	"strings"
	"time"
)

type Client struct {
	Start int64
	Stop  int64
	Path  []string
	Res   http.ResponseWriter
	Req   *http.Request
	User  *User
}

// response writer

func (c *Client) WriteJson(v interface{}) {

	data, err := json.Marshal(v)
	if err != nil {
		fmt.Fprint(c.Res, err.Error())
		return
	}

	c.Res.Header().Add(`Content-Type`, `application/json`)
	c.Res.Write(data)
}

// reading headers

func (c *Client) AcceptLanguage() (referer string) {
	if slice := c.Req.Header["Accept-Language"]; len(slice) > 0 {
		referer = slice[0]
	}
	return
}

func (c *Client) UserAgent() (referer string) {
	if slice := c.Req.Header["User-Agent"]; len(slice) > 0 {
		referer = slice[0]
	}
	return
}

func (c *Client) Referer() (referer string) {
	if slice := c.Req.Header["Referer"]; len(slice) > 0 {
		referer = slice[0]
	}
	return
}

func (c *Client) Ip() (ip string) {
	if remote := strings.Split(c.Req.RemoteAddr, ":"); len(remote) > 0 {
		ip = remote[0]
	}
	return
}

// cookie

func (c *Client) GetCookie(name string) (value string) {

	if cookie, err := c.Req.Cookie(name); err == nil {
		value = cookie.Value
	}
	return
}

func (c *Client) SetCookie(name, value string) {

	http.SetCookie(c.Res, &http.Cookie{
		Name:     name,
		Value:    value,
		Path:     "/",
		Expires:  time.Now().Local().AddDate(0, 0, 365),
		HttpOnly: true,
	})
}

// redirect

func (c *Client) Redirect(uri string) {

	c.Res.Header().Set("Location", uri)

	if c.Req.Method == "POST" {
		c.Res.WriteHeader(http.StatusSeeOther)
	} else {
		c.Res.WriteHeader(http.StatusTemporaryRedirect)
	}
}

// reading parameters

func (c *Client) FormValueUint(name string) uint {
	return utils.ToUint(c.Req.FormValue(name))
}

func (c *Client) FormValueInt(name string) int {
	return utils.ToInt(c.Req.FormValue(name))
}

func (c *Client) FormValueUint64(name string) uint64 {
	return utils.ToUint64(c.Req.FormValue(name))
}

func (c *Client) FormValueInt64(name string) int64 {
	return utils.ToInt64(c.Req.FormValue(name))
}

//

func (c *Client) Unmarshal(v interface{}) {

	defer func() {
		if e := recover(); e != nil {
			fmt.Printf("Panic! %v\n", e)
		}
	}()

	c.Req.ParseForm()
	rv := reflect.ValueOf(v)

	// fmt.Println(c.Req.Form)

	if rv.Kind() != reflect.Ptr || rv.IsNil() {
		return
	}

	rv = rv.Elem()
	typ := rv.Type()

	if typ.Kind() != reflect.Struct {
		return
	}

	for i := 0; i < typ.NumField(); i++ {

		field := typ.Field(i)

		if field.PkgPath != "" {
			continue
		}

		tagVal := field.Tag.Get("form")
		if tagVal != "" {

			formArray := c.Req.Form[tagVal]
			if len(formArray) > 0 {

				value := rv.FieldByName(field.Name)
				formVal := formArray[0]

				switch value.Kind() {
				case reflect.String:
					value.SetString(formVal)

				case reflect.Float32, reflect.Float64:
					value.SetFloat(utils.ToFloat64(formVal))

				case reflect.Int, reflect.Int64:
					value.SetInt(utils.ToInt64(formVal))

				case reflect.Uint, reflect.Uint64:
					value.SetUint(utils.ToUint64(formVal))

				case reflect.Slice:

					strArray := strings.Split(formVal, ",")
					tmp := make([]string, 0, len(strArray))

					for _, str := range strArray {
						if str != "" {
							tmp = append(tmp, str)
						}
					}

					strArray = tmp

					switch value.Type().String() {
					case "[]string":
						value.Set(reflect.ValueOf(strArray))

					case "[]int":
						value.Set(reflect.ValueOf(utils.ToSliceInt(strArray)))

					case "[]int64":
						value.Set(reflect.ValueOf(utils.ToSliceInt64(strArray)))

					case "[]uint":
						value.Set(reflect.ValueOf(utils.ToSliceUint(strArray)))

					case "[]uint64":
						value.Set(reflect.ValueOf(utils.ToSliceUint64(strArray)))

					case "[]float32":
						value.Set(reflect.ValueOf(utils.ToSliceFloat32(strArray)))

					case "[]float64":
						value.Set(reflect.ValueOf(utils.ToSliceFloat64(strArray)))
					}
				}
			}
		}
	}
}

// errors

func (c *Client) MethodNotAllowed() {

	c.Res.WriteHeader(http.StatusMethodNotAllowed)
	page := Render{Res: c.Res, Tmpl: "errors/405.html"}
	page.Render()
}

func (c *Client) ServiceUnavailable() {

	c.Res.WriteHeader(http.StatusServiceUnavailable)
	page := Render{Res: c.Res, Tmpl: "errors/503.html"}
	page.Render()
}

func (c *Client) NotFound() {

	c.Res.WriteHeader(http.StatusNotFound)
	page := Render{Res: c.Res, Tmpl: "errors/404.html"}
	page.Render()
}

func (c *Client) InternalServerError() {

	c.Res.WriteHeader(http.StatusInternalServerError)
	page := Render{Res: c.Res, Tmpl: "errors/500.html"}
	page.Render()
}
