package model

const (
	TYPE_ADMIN    = 1
	TYPE_EMPLOYEE = 2
	TYPE_USER     = 3
)

type (
	User struct {
		Id         int64  `json:"id"`
		Type       int    `json:"type"`
		Name       string `json:"name"`
		Login      string `json:"login"`
		Passphrase string `json:"passphrase"`
	}
)
