package model

import (
	"../config"
	"../template_cache"
	"bytes"
	"fmt"
	"html/template"
	"net/http"
)

type (
	PageHead struct {
		Page        string
		Title       string
		Keywords    string
		Description string
	}

	PageHeader struct {
		User *User
	}

	PageFooter struct {
		Page string
	}

	PageData struct {
		Head   template.HTML
		Header template.HTML
		Footer template.HTML
		Data   interface{}
	}

	Render struct {
		Res      http.ResponseWriter
		EditMode int
		Tmpl     string
		Lang     string
		Head     PageHead
		Header   PageHeader
		Footer   PageFooter
		Data     interface{}
	}
)

const (
	TEMPLATE_PATH = `../template/`
)

var (
	cache *template_cache.CacheTemplate
)

func init() {

	conf := config.GetConfig()

	if conf.Http.Template != "" {
		cache = template_cache.CreateCache(conf.Http.Template)
	} else {
		cache = template_cache.CreateCache(TEMPLATE_PATH)
	}
}

func (render *Render) Render() {

	if render.Lang == "" {
		render.Lang = "ru"
	}

	tmplPath := render.Lang + "/" + render.Tmpl

	tmpl, _ := cache.Get(tmplPath)

	if tmpl == nil {
		fmt.Fprint(render.Res, tmplPath+" not found")
		return
	}

	var head, header, footer bytes.Buffer

	tmplHeadPath := render.Lang + "/_head.html"
	tmplHeaderPath := render.Lang + "/_header.html"
	tmplFooterPath := render.Lang + "/_footer.html"

	if render.EditMode != 0 {
		tmplHeadPath = render.Lang + "/_head_edit.html"
		tmplHeaderPath = render.Lang + "/_header_edit.html"
		tmplFooterPath = render.Lang + "/_footer_edit.html"
	}

	tmplHead, _ := cache.Get(tmplHeadPath)
	tmplHeader, _ := cache.Get(tmplHeaderPath)
	tmplFooter, _ := cache.Get(tmplFooterPath)

	tmplHead.Execute(&head, &render.Head)
	tmplHeader.Execute(&header, &render.Header)
	tmplFooter.Execute(&footer, &render.Footer)

	pageData := PageData{
		Head:   template.HTML(head.String()),
		Header: template.HTML(header.String()),
		Footer: template.HTML(footer.String()),
		Data:   &render.Data,
	}

	err := tmpl.Execute(render.Res, &pageData)
	if err != nil {
		fmt.Println(err)
	}
}
