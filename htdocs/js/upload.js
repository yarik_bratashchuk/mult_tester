
window._upl_cnt = 0;

window._cur_uploader = {};

function InitUploadImgEx( container, browse_button, drop_element, cb_img_url, cb_error, cb_start, cb_progress, multi_selection )
{
    DestroyUploadImg(container);

    if( multi_selection === 0 ) multi_selection = false;
    else multi_selection = true;

    var uploader = new plupload.Uploader({
        runtimes: 'html5,flash,silverlight,browserplus',
        browse_button: browse_button,
        container: container,
        drop_element: drop_element,
        multi_selection: multi_selection,
        file_data_name: 'file',
        max_file_size: '10mb',
        url: '/upload',
        required_features : 'send_browser_cookies',
        flash_swf_url: '/js/plupload/Moxie.swf',
        silverlight_xap_url: '/js/plupload/Moxie.xap',
        filters: [
            { title: "Image files", extensions: "jpg,jpeg,png,bmp,gif" }
        ]
    });

    window._cur_uploader[container] = uploader;

    uploader.bind('Init', function(up, params) {
        console.log( params.runtime );
    });

    uploader.init();

    uploader.bind('FilesAdded', function(up, files) {
        up.refresh();
        uploader.start();
        if( cb_start ) cb_start( files )
    });

    uploader.bind('UploadProgress', function(up, file) {
        if( cb_progress ) cb_progress( file );
    });

    uploader.bind('Error', function(up, err) {
        console.log( "Error: ", err.message );
        up.refresh();
    });

    uploader.bind('FileUploaded', function(up, file, res) {
        if( res.status == 200 ) {
            var img_url = res.response;
            if( cb_img_url ) cb_img_url( img_url, file );
        } else {
            if( cb_error ) cb_error( file );
            alert( "Произошла ошибка при загрузки картинки. Пожалуйста, попробуйте позже." );
            console.log( res, file );
        }
    });
}

function DestroyUploadImg(container) {
    console.log( window._cur_uploader[container] );
    if( window._cur_uploader[container] ) {
        window._cur_uploader[container].destroy();
        window._cur_uploader[container] = null;
    }
}
